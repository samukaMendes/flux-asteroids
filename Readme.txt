=== Instructions ===
Destroy all Asteroids, before you be destroyed!

Move = Arros / WASD;
Shoot = Space / Enter;

=== Extra informations ===
Made with Unity 2018.1.1f1

=== Develop time ===
Developed in 4 days.

=== Assets de terceiros ===
-- Fonts
	Pixel Operator
	https://www.dafont.com/pt/pixel-operator.font

-- BGM
	We are number one (Chiptune remix)
	https://www.youtube.com/watch?v=64Wc9YRKC5o

	Take on me (Chiptune cover)
	https://www.youtube.com/watch?v=xAzyU7h9LQg

	Thriller (Commodore 64 chiptune cover)
	https://www.youtube.com/watch?v=-me5AGrIvOo

	Ocean Man (Chiptune Cover)
	https://www.youtube.com/watch?v=NQLHwyY9S7w

-- Art
	Spaceships (Player e Inimigos)
	https://www.colourbox.com/vector/set-of-8-bit-color-space-monsters-vector-10003637

	Level 1 (Background)
	https://wall.alphacoders.com/big.php?i=885542

	Level 2 (Background)
	https://magical-mama.deviantart.com/art/FREE-Purple-Space-Background-139763515

	Asteroid
	https://spencer2124.wordpress.com/2014/06/24/illustration-updates/

	Laser Beam (Can't access source site, only photo)
	https://farm4.staticflickr.com/3835/14625993337_75e5c27b8d_o.png

=== Credits ===
Made by Samuel Mendes