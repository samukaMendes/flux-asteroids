﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPlayer : Movement
{
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        Vector3 vertical = transform.forward * Input.GetAxis("Vertical");
        Vector3 horizontal = transform.up * Input.GetAxis("Horizontal");

        Move(vertical);
        Rotate(horizontal);
    }
}
