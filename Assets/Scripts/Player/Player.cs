﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MovementPlayer))]
public class Player : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    private BulletSpawner m_bulletSpawner;
    public BulletSpawner BulletSpawner { get { return m_bulletSpawner; } }

    [SerializeField]
    private List<Canvas> m_canvas;
    public Canvas[] Canvas { get { return m_canvas.ToArray(); } }

    [SerializeField]
    private GunBarrelManager m_gunBarrelsManager;
    public GunBarrelManager GunBarrelsManager { get { return m_gunBarrelsManager; } }

    private void Reset()
    {
        if (m_gunBarrelsManager == null)
            m_gunBarrelsManager = GetComponentInChildren<GunBarrelManager>();

        GetCanvas();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Submit"))
        {
            UPGunBarrels[] activeBarrels = m_gunBarrelsManager.ActiveBarrels;
            for (int i = 0; i < activeBarrels.Length; i++)
            {
                m_bulletSpawner.Shoot(activeBarrels[i].transform, FX.Shoot_Player);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (GameManager.Instance.IsInvincible)
            return;

        SoundManager.Instance.AttachFX(gameObject, FX.Explosion_Player);
        GameManager.Instance.Die();
    }

    public void LevelUp()
    {
        m_gunBarrelsManager.UpdateBarrels();
        m_bulletSpawner.QueuePoolUpdate(m_gunBarrelsManager.TotalAmmoNeed);
    }

    [ContextMenu("Get Canvas")]
    private void GetCanvas()
    {
        m_canvas = new List<Canvas>(GetComponentsInChildren<Canvas>());
    }
}