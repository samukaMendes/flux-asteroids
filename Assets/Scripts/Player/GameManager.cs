﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    [Header("Game Config")]
    [SerializeField, ReadOnly]
    private bool m_inGame = false;
    public bool InGame { get { return m_inGame; } }

    [SerializeField]
    private SceneConfig m_sceneConfig;
    public SceneConfig SceneConfig { get { return m_sceneConfig; } }

    [SerializeField]
    private BGM m_levelMusic;

    [SerializeField]
    private BGM m_victoryMusic;

    [SerializeField]
    private string m_victoryText = "All Asteroids destroyed!";
    public string VictoryText { get { return m_victoryText; } }
    
    [SerializeField]
    private BGM m_defeatMusic;

    [SerializeField]
    private string m_defeatText = "You die!";
    public string DefeatText { get { return m_defeatText; } }

    [SerializeField]
    private Player m_player;
    public Player Player { get { return m_player; } }

    public Vector3 PlayerPosition { get { return Player.transform.position; } }

    [Header("UI Settings")]
    [SerializeField]
    private GUIGameOver m_guiGameOver;

    [SerializeField]
    private GUINameInsert m_guiNameInsert;

    [Header("Life config")]
    [SerializeField]
    private float m_respawnDelay = 1f;

    [SerializeField]
    private float m_invincibilityDuration = 2f;

    [SerializeField]
    private float m_invincibilityEffectInterval = 0.5f;

    [SerializeField, ReadOnly]
    private bool m_isInvincible = false;
    public bool IsInvincible { get { return m_isInvincible; } }

    [SerializeField, ReadOnly]
    private int m_life = 3;
    public int Life { get { return m_life; } }

    [Header("Level configs")]
    [SerializeField]
    private LevelUpEffect m_levelUpEffects;

    [SerializeField, ReadOnly]
    private int m_currentLevel = 1;
    public int CurrentLevel { get { return m_currentLevel; } }

    [SerializeField, ReadOnly]
    private float m_currentXP = 0f;
    public float CurrentXP { get { return m_currentXP; } }

    [Header("Points config")]
    [SerializeField]
    private PointsConfig m_pointsConfig;
    public PointsConfig PointsConfig { get { return m_pointsConfig; } }

    [SerializeField, ReadOnly]
    private int m_currentScore = 0;
    public int CurrentScore { get { return m_currentScore; } }

    public EffectContainer LevelEffect { get { return m_levelUpEffects.Effects[m_currentLevel - 1]; } }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        if (m_player == null)
            m_player = FindObjectOfType<Player>();
    }

    private void Start()
    {
        Player.gameObject.SetActive(m_inGame);
    }

    public void Die()
    {
        if (m_life > 1)
        {
            m_life--;
            StartCoroutine(Respawn(m_respawnDelay));
            Player.gameObject.SetActive(false);
        }
        else
        {
            GameOver();
        }
    }

    public void GameStart()
    {
        m_inGame = true;
        Player.gameObject.SetActive(true);

        SoundManager.Instance.SwitchBGM(m_levelMusic, 1f);
    }

    public void GameOver()
    {
        if (!m_inGame)
            return;

        m_inGame = false;
        Player.gameObject.SetActive(false);

        SoundManager.Instance.SwitchBGM(m_defeatMusic, 1f);
        m_guiGameOver.UpdateTitle(m_defeatText);

        if (PointsConfig.IsOnHighScore(m_currentScore))
        {
            string pointPosition = "You made to " + PointsConfig.HighScorePosition(m_currentScore) + "º - " + m_currentScore;
            m_guiNameInsert.Active(true, m_defeatText, pointPosition);
        }
        else
            ShowGameOver();
    }

    public void GameComplete()
    {
        if (!m_inGame)
            return;

        m_inGame = false;
        Player.gameObject.SetActive(false);

        SoundManager.Instance.SwitchBGM(m_victoryMusic, 1f);
        m_guiGameOver.UpdateTitle(m_victoryText);

        if (PointsConfig.IsOnHighScore(m_currentScore))
        {
            string pointPosition = "You made to " + PointsConfig.HighScorePosition(m_currentScore) + "º - " + m_currentScore;
            m_guiNameInsert.Active(true, m_victoryText, pointPosition);
        }
        else
            ShowGameOver();
    }

    private IEnumerator InvincibleEffect()
    {
        float time = 0f;
        while (time < m_invincibilityDuration)
        {
            if (time % (m_invincibilityEffectInterval * 2) < m_invincibilityEffectInterval)
            {
                for (int i = 0; i < Player.Canvas.Length; i++)
                {
                    Player.Canvas[i].enabled = false;
                }
            }
            else
            {
                for (int i = 0; i < Player.Canvas.Length; i++)
                {
                    Player.Canvas[i].enabled = true;
                }
            }

            time += Time.deltaTime;
            yield return null;
        }

        for (int i = 0; i < Player.Canvas.Length; i++)
        {
            Player.Canvas[i].enabled = true;
        }
        m_isInvincible = false;
    }

    private IEnumerator Respawn(float delay)
    {
        yield return new WaitForSeconds(delay);

        m_isInvincible = true;
        StartCoroutine(InvincibleEffect());
        Player.gameObject.SetActive(true);
    }

    public void AddScore(int points, float xp = 0f)
    {
        m_currentScore += points;
        m_currentXP += xp;

        if (m_currentXP >= LevelEffect.XpToNextLevel && m_currentLevel < m_levelUpEffects.Effects.Length)
        {
            LevelUp();
        }
    }

    private void LevelUp()
    {
        m_currentLevel++;
        m_currentXP = 0;

        Player.LevelUp();
    }

    public void InsertName(string name)
    {
        PointsConfig.RegisterHighScore(name, m_currentScore);

        ShowGameOver();
    }

    private void ShowGameOver()
    {
        m_guiGameOver.UpdateHighScore(m_pointsConfig);
        m_guiGameOver.Active(true);
    }
}
