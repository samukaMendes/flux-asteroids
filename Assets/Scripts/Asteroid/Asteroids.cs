﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MovementAsteroid))]
public class Asteroids : MonoBehaviour
{
    [SerializeField]
    private float m_scaleFactor = 0.5f;

    [SerializeField, ReadOnly]
    private int m_currentLayer = 0;

    [SerializeField, ReadOnly]
    private GameObject[] m_subAsteroids;

    [SerializeField]
    private int m_scoreReward = 100;

    private Vector3 m_originalScale;

    public AsteroidGenerator AsteroidGenerator { get; private set; }

    private void Awake()
    {
        m_originalScale = transform.localScale;

        if (AsteroidGenerator == null)
            AsteroidGenerator = GetComponentInParent<AsteroidGenerator>();
    }

    public void Active(Vector3 position, int currentLayer)
    {
        m_currentLayer = currentLayer;

        transform.rotation = Quaternion.identity;
        transform.localScale = (m_originalScale * m_scaleFactor) * m_currentLayer;
        transform.position = position;

        gameObject.SetActive(true);
    }

    public void Desativate()
    {
        m_subAsteroids = AsteroidGenerator.RequestSubAsteroids();
        if (m_currentLayer > 1)
        {
            for (int i = 0; i < m_subAsteroids.Length; i++)
            {
                Asteroids asteroid = m_subAsteroids[i].GetComponent<Asteroids>();
                asteroid.Active(transform.position, m_currentLayer - 1);
            }
        }

        SoundManager.Instance.AttachFX(gameObject, FX.Explosion_Asteroid);

        gameObject.SetActive(false);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<Bullet>() != null)
        {
            GameManager.Instance.AddScore(m_scoreReward);
            Desativate();
        }
    }
}
