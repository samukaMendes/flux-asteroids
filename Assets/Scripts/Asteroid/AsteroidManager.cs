﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Pooler))]
public class AsteroidManager : MonoBehaviour {

    public Pooler Pooler { get; private set; }

    private bool m_calledStart = false;

    private void Awake()
    {
        if (Pooler == null)
            Pooler = GetComponent<Pooler>();
    }

    private void OnEnable()
    {
        Pooler.PoolGenerated += PoolGenerated;
        Pooler.AllDisabled += AllDisabled;
        Pooler.AllEnabled += AllEnabled;
    }

    private void OnDisable()
    {
        Pooler.PoolGenerated -= PoolGenerated;
        Pooler.AllDisabled -= AllDisabled;
        Pooler.AllEnabled -= AllEnabled;
    }

    private void PoolGenerated()
    {
        Pooler.ActivateAll();
    }

    private void AllDisabled()
    {
        if (!GameManager.Instance.InGame)
            return;

        GameManager.Instance.GameComplete();
    }

    private void AllEnabled()
    {
        if (GameManager.Instance.InGame)
            return;

        if (!m_calledStart)
        {
            GameManager.Instance.GameStart();
            m_calledStart = true;
        }
    }
}
