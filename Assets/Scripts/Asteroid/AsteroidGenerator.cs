﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Pooler))]
public class AsteroidGenerator : MonoBehaviour {
    [SerializeField]
    private int m_maxLayers = 4;

    [SerializeField]
    private int m_pieces = 2;

    public int MaxPieces
    {
        get
        {
            int total = 1;
            int lastPieceCreated = 1;

            for (int i = 1; i < m_maxLayers; i++)
            {
                total += lastPieceCreated * m_pieces;
                lastPieceCreated = lastPieceCreated * m_pieces;
            }

            return total;
        }
    }

    public Pooler Pooler { get; private set; }

    private void Awake()
    {
        if (Pooler == null)
            Pooler = GetComponent<Pooler>();

        Pooler.UpdatePoolLimit(MaxPieces, false);
    }

    private void OnEnable()
    {
        Pooler.PoolGenerated += PoolGenerated;
        Pooler.AllDisabled += AllDisabled;
    }

    private void OnDisable()
    {
        Pooler.PoolGenerated -= PoolGenerated;
        Pooler.AllDisabled -= AllDisabled;
    }

    private void PoolGenerated()
    {
        Vector3 position = Camera.main.ViewportToWorldPoint(new Vector3(Random.Range(0f, 1f), 0, Random.Range(0f, 1f)));
        position.y = 0;
        Asteroids asteroid = Pooler.ObjectPool[0].GetComponent<Asteroids>();
        asteroid.Active(position, m_maxLayers);
    }

    private void AllDisabled()
    {
        gameObject.SetActive(false);
    }

    

    public GameObject[] RequestSubAsteroids()
    {
        return Pooler.AvaliableObjects(m_pieces);
    }
}
