﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementAsteroid : Movement
{
    [SerializeField, Range(-360f, 360f)]
    private float m_minAngle = 0f;

    [SerializeField, Range(-360f, 360f)]
    private float m_maxAngle = 360f;

    private void OnEnable()
    {
        if(m_maxAngle < m_minAngle)
        {
            float temp = m_maxAngle;
            m_maxAngle = m_minAngle;
            m_minAngle = temp;
        }

        float random = Random.Range(m_minAngle, m_maxAngle);
        transform.eulerAngles = Vector3.up * random;
        Rigidbody.velocity = Vector3.zero;
        Move(transform.forward, ForceMode.Impulse);
    }
}
