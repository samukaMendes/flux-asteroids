﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDScore : MonoBehaviour
{
    [SerializeField]
    private Text m_score;

    public RectTransform RectTransform { get; private set; }

    private void Awake()
    {
        if (RectTransform == null)
            RectTransform = GetComponent<RectTransform>();
    }

    private void Update()
    {
        RectTransform.localScale = GameManager.Instance.InGame ? Vector3.one : Vector3.zero;

        m_score.text = GameManager.Instance.CurrentScore.ToString();
    }
}
