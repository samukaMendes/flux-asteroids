﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDHighscoreContainer : MonoBehaviour
{
    [SerializeField]
    private Text m_text;
    public Text Text { get { return m_text; } }
}
