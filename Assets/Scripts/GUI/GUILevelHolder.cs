﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GUILevelHolder : MonoBehaviour
{
    [SerializeField]
    private Text m_levelName;
    public Button LevelName { get { return m_levelButton; } }

    [SerializeField]
    private Button m_levelButton;
    public Button LevelButton { get { return m_levelButton; } }

    [SerializeField, ReadOnly]
    private SceneContainer m_levelInformation;

    private void Awake()
    {
        if (m_levelName == null)
            m_levelName = GetComponentInChildren<Text>();

        if (m_levelButton == null)
            m_levelButton = GetComponentInChildren<Button>();
    }

    public void Activate(SceneContainer levelInformation)
    {
        m_levelInformation = levelInformation;
        UpdateLevelName(m_levelInformation.LevelName);
        UpdateLevelImage(m_levelInformation.LevelImage);
    }

    public void UpdateLevelName(string name)
    {
        m_levelName.text = name;
    }

    public void UpdateLevelImage(Sprite sprite)
    {
        m_levelButton.image.sprite = sprite;
    }

    public void SelectLevel()
    {
        SceneManager.LoadScene(m_levelInformation.LevelBuildIndex);
    }
}
