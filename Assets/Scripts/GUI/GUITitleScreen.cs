﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GUITitleScreen : MonoBehaviour
{
    [SerializeField]
    private SceneConfig m_sceneConfig;
    public SceneConfig SceneConfig { get { return m_sceneConfig; } }

    [SerializeField]
    private BGM m_menuMusic = BGM.Main_Menu;

    [SerializeField]
    private GameObject m_pressStart;

    [SerializeField]
    private GameObject m_menuChoices;

    [SerializeField]
    private GUILevelSelect m_levelSelect;

    private void Awake()
    {
        SoundManager.Instance.SwitchBGM(m_menuMusic, 1f);
        m_pressStart.SetActive(true);
        m_menuChoices.SetActive(false);
    }

    private void Update()
    {
        if (m_pressStart.activeInHierarchy && Input.GetButtonDown("Submit"))
        {
            m_pressStart.SetActive(false);
            m_menuChoices.gameObject.SetActive(true);
        }
    }

    public void RandomLevel()
    {
        SceneManager.LoadScene(m_sceneConfig.RandomScene());
    }

    public void LevelSelect()
    {
        m_menuChoices.gameObject.SetActive(false);
        m_levelSelect.gameObject.SetActive(true);
    }
}
