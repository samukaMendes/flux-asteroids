﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUINameInsert : MonoBehaviour
{
    [SerializeField]
    private Text m_messageText;

    [SerializeField]
    private InputField m_inputField;

    [SerializeField]
    private Text m_highscorePosition;

    private void Awake()
    {
        Active(false);
    }

    public void Active(bool active, string message = "", string position = "")
    {
        m_messageText.text = message;
        m_highscorePosition.text = position;

        gameObject.SetActive(active);
    }

    public void SubmitName()
    {
        GameManager.Instance.InsertName(m_inputField.text);
        Active(false);
    }
}
