﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Pooler))]
public class GUILevelSelect : MonoBehaviour
{
    [SerializeField]
    private GUITitleScreen m_titleScreen;

    public Pooler Pooler { get; private set; }

    public EventSystem EventSystem { get; private set; }

    private void Awake()
    {
        if (Pooler == null)
            Pooler = GetComponent<Pooler>();

        if (EventSystem == null)
            EventSystem = FindObjectOfType<EventSystem>();

        if (m_titleScreen == null)
            m_titleScreen = GetComponentInParent<GUITitleScreen>();

        gameObject.SetActive(false);
    }

    private void Start()
    {
        Pooler.PoolGenerated += PoolGenerated;
        Pooler.UpdatePoolLimit(m_titleScreen.SceneConfig.Levels.Length);
    }

    private void PoolGenerated()
    {
        Pooler.PoolGenerated -= PoolGenerated;

        GUILevelHolder[] holders = GetHolders();
        for (int i = 0; i < holders.Length; i++)
        {
            holders[i].Activate(m_titleScreen.SceneConfig.Levels[i]);
        }
        
        Pooler.ActivateAll();
        EventSystem.SetSelectedGameObject(holders[0].LevelButton.gameObject);
    }

    private GUILevelHolder[] GetHolders()
    {
        GUILevelHolder[] holders = new GUILevelHolder[Pooler.ObjectPoolLimit];
        for (int i = 0; i < Pooler.ObjectPool.Count; i++)
        {
            holders[i] = Pooler.ObjectPool[i].GetComponent<GUILevelHolder>();
        }

        return holders;
    }
}
