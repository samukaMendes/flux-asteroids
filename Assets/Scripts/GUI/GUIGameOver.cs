﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Pooler))]
public class GUIGameOver : MonoBehaviour
{
    [SerializeField]
    private Text m_title;

    [SerializeField, ReadOnly]
    private PointsConfig m_pointsConfig;

    public RectTransform RectTransform { get; private set; }

    public Pooler HighscorePooler { get; private set; }

    private void Awake()
    {
        if (RectTransform == null)
            RectTransform = GetComponent<RectTransform>();

        if (HighscorePooler == null)
            HighscorePooler = GetComponent<Pooler>();
    }

    private void Start()
    {
        gameObject.SetActive(false);
    }

    private void PoolGenerated()
    {
        Debug.Log("Activate All");
        HighscorePooler.ActivateAll();
        HighscorePooler.PoolGenerated -= PoolGenerated;
    }

    public void UpdateTitle(string title)
    {
        m_title.text = title;
    }

    public void UpdateHighScore(PointsConfig pointsConfig)
    {
        HighscorePooler.PoolGenerated += PoolGenerated;
        if (pointsConfig.MaxRegisters != HighscorePooler.ObjectPoolLimit)
            HighscorePooler.UpdatePoolLimit(pointsConfig.MaxRegisters);

        Text[] texts = GetTextFromPool();
        for (int i = 0; i < texts.Length; i++)
        {
            texts[i].text = (i + 1) + "º - " + pointsConfig.SavedScore[i].Name + ": " + pointsConfig.SavedScore[i].Score;
        }
    }

    public void Active(bool active)
    {
        gameObject.SetActive(active);
    }

    private Text[] GetTextFromPool()
    {
        Text[] texts = new Text[HighscorePooler.ObjectPoolLimit];
        for (int i = 0; i < HighscorePooler.ObjectPool.Count; i++)
        {
            HUDHighscoreContainer highscoreContainer = HighscorePooler.ObjectPool[i].GetComponent<HUDHighscoreContainer>();
            texts[i] = highscoreContainer.Text;
        }

        return texts;
    }

    public void ReloadScene()
    {
        int currentScene = SceneManager.GetActiveScene().buildIndex;
        Debug.Log(currentScene);
        SceneManager.LoadScene(currentScene);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene(GameManager.Instance.SceneConfig.Menu.LevelBuildIndex);
    }
}
