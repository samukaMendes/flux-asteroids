﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Scene config", menuName = "Scene Select/Scene config", order = 20)]
public class SceneConfig : ScriptableObject
{
    [SerializeField]
    private SceneContainer m_menu;
    public SceneContainer Menu { get { return m_menu; } }

    [SerializeField]
    private SceneContainer[] m_levels;
    public SceneContainer[] Levels { get { return m_levels; } }

    public int RandomScene()
    {
        return m_levels[Random.Range(0, m_levels.Length)].LevelBuildIndex;
    }
}
