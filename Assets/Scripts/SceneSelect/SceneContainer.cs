﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public class SceneContainer
{
    [SerializeField]
    private string m_levelName;
    public string LevelName { get { return m_levelName; } }

    [SerializeField]
    private Sprite m_levelImage;
    public Sprite LevelImage { get { return m_levelImage; } }

    [SerializeField]
    private int m_levelBuildIndex;
    public int LevelBuildIndex { get { return m_levelBuildIndex; } }
}
