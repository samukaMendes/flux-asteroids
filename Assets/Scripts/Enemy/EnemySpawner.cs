﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Pooler))]
public class EnemySpawner : MonoBehaviour
{

    [SerializeField]
    private BulletSpawner m_bulletSpawner;

    [SerializeField]
    private bool m_autoSpawn = true;

    [SerializeField, Range(0f, 1f)]
    private float m_changeOfSpawn = 0.1f;

    public Pooler Pooler { get; private set; }

    private void Start()
    {
        if (Pooler == null)
            Pooler = GetComponent<Pooler>();
    }

    private void Update()
    {
        if (!GameManager.Instance.InGame)
            return;

        if(m_autoSpawn && Time.time % 1 >= 0.98)
        {
            if (m_changeOfSpawn >= Random.Range(0f, 1f))
                Spawn();
        }
    }

    public void Spawn()
    {
        GameObject enemyGameObject = Pooler.AvaliableObject;
        if (enemyGameObject != null)
        {
            Enemy enemy = enemyGameObject.GetComponent<Enemy>();
            Vector3 viewportPosition = new Vector3(Random.Range(0f,1f), Random.Range(0f, 1f), 0);
            Vector3 worldpPos = Camera.main.ViewportToWorldPoint(viewportPosition);
            worldpPos.y = 0f;
            enemy.Active(worldpPos, Quaternion.identity, m_bulletSpawner);
        }
    }
}
