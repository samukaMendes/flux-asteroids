﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementEnemy : Movement
{
    [Header("Rotation randomness")]
    [SerializeField]
    private bool m_randomForward = true;

    [SerializeField, Range(0f, 360f)]
    private float m_randomForwardMinAngle = 0f;

    [SerializeField, Range(0f, 360f)]
    private float m_randomForwardMaxAngle = 360f;

    [SerializeField, Range(0f, 1f)]
    private float m_shootMissChance = 0.15f;

    [Header("Side ways pattern")]
    [SerializeField]
    private bool m_changePattern = true;

    [SerializeField]
    private AnimationCurve[] m_movimentPath;

    private int m_currentCurve = 0;

    [SerializeField, ReadOnly]
    private float m_leapseTime = 0f;

    [SerializeField, ReadOnly]
    private float m_currentCurveDuration = 0f;

    private void OnEnable()
    {
        if(m_randomForward)
        {
            float random = Random.Range(m_randomForwardMinAngle, m_randomForwardMaxAngle);
            transform.eulerAngles = Vector3.up * random;
            Rigidbody.velocity = Vector3.zero;
        }
    }

    protected override void Update()
    {
        base.Update();
        m_leapseTime += Time.deltaTime;
        if (m_changePattern && m_leapseTime >= m_currentCurveDuration)
        {
            m_currentCurve = m_currentCurve + 1 >= m_movimentPath.Length ? 0 : m_currentCurve + 1;
            m_currentCurveDuration = m_movimentPath[m_currentCurve].keys[m_movimentPath[m_currentCurve].length - 1].time;
            m_leapseTime = 0f;
        }
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        AnimationMove();
    }

    private void AnimationMove()
    {
        AnimationCurve animationCurve = m_movimentPath[m_currentCurve];
        float value = animationCurve.Evaluate(m_leapseTime);
        Move(transform.right * value, ForceMode.Impulse);
        Move(transform.forward);
    }

    public void FollowPlayer(Vector3 position)
    {
        Vector3 heading = position - transform.position;
        float radius = heading.magnitude;
        float angle = Random.Range(-m_shootMissChance, m_shootMissChance) * 15f;

        heading.x += Mathf.Sin(Mathf.Deg2Rad * angle) * radius;
        heading.z += Mathf.Sin(Mathf.Deg2Rad * angle) * radius;

        Rigidbody.rotation = Quaternion.Slerp(Quaternion.FromToRotation(Vector3.forward, heading), Rigidbody.rotation, (m_shootMissChance * 100f) * Time.fixedDeltaTime);
    }
}
