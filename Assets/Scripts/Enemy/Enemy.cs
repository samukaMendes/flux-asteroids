﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MovementEnemy))]
public class Enemy : MonoBehaviour {
    [SerializeField]
    private Transform m_shootPoint;

    [SerializeField]
    private float m_lifetime = 3f;

    [SerializeField, ReadOnly]
    private float m_lifetimeLeapse = 0f;

    [SerializeField, Range(0f,1f)]
    private float m_changeOfShoot = 1f;

    [Header("Player configuration")]
    [SerializeField]
    private int m_scoreReward = 100;

    [SerializeField]
    private float m_xpReward = 100f;

    [SerializeField, ReadOnly]
    private BulletSpawner m_bulletSpawner;

    public MovementEnemy MovementEnemy { get; private set; }

    private void Awake()
    {
        if (MovementEnemy == null)
            MovementEnemy = GetComponent<MovementEnemy>();
    }

    private void Update()
    {
        MovementEnemy.FollowPlayer(GameManager.Instance.PlayerPosition);
        if (m_lifetimeLeapse >= m_lifetime)
            Desativate();
        else
            m_lifetimeLeapse += Time.deltaTime;

        if (Time.time % 1 >= 0.98 && Random.Range(0f, 1f) <= m_changeOfShoot)
        {
            m_bulletSpawner.Shoot(m_shootPoint);
        }
    }
    
    public void Active(Vector3 position, Quaternion rotation, BulletSpawner bulletSpawner)
    {
        transform.position = position;
        transform.rotation = rotation;
        m_bulletSpawner = bulletSpawner;

        gameObject.SetActive(true);
    }

    public void Desativate()
    {
        m_lifetimeLeapse = 0f;
        SoundManager.Instance.AttachFX(gameObject, FX.Explosion_Enemy);
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<Bullet>() != null)
        {
            GameManager.Instance.AddScore(m_scoreReward, m_xpReward);
            Desativate();
        }   
    }
}
