﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level Effects - 1", menuName = "Level System/Create Level Effect asset", order = 20)]
public class LevelUpEffect : ScriptableObject
{
    [SerializeField]
    protected EffectContainer[] m_effects;
    public EffectContainer[] Effects { get { return m_effects; } }
}
