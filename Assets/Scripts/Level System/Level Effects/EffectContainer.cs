﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EffectContainer
{
    [SerializeField]
    protected float m_xpToNextLevel = 100f;
    public float XpToNextLevel { get { return m_xpToNextLevel; } }
}
