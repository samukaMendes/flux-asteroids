﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunBarrelManager : MonoBehaviour
{
    [SerializeField, ReadOnly]
    private UPGunBarrels[] m_barrels;

    public UPGunBarrels[] ActiveBarrels
    {
        get { return GetComponentsInChildren<UPGunBarrels>(); }
    }

    public int TotalAmmoNeed
    {
        get
        {
            UPGunBarrels[] activeBarrels = ActiveBarrels;
            int ammo = 0;
            for (int i = 0; i < activeBarrels.Length; i++)
            {
                ammo += activeBarrels[i].MaxAmmo;
            }

            return ammo;
        }
    }

    private void Reset()
    {
        FindBarrels();
    }

    private void Start()
    {
        UpdateBarrels();
    }

    public void UpdateBarrels()
    {
        for (int i = 0; i < m_barrels.Length; i++)
        {
            if (m_barrels[i].AvaliableInLevels.Contains(GameManager.Instance.CurrentLevel))
                m_barrels[i].gameObject.SetActive(true);
            else
                m_barrels[i].gameObject.SetActive(false);
        }
    }

    [ContextMenu("Find Barrels")]
    private void FindBarrels()
    {
        m_barrels = GetComponentsInChildren<UPGunBarrels>();
    }
}
