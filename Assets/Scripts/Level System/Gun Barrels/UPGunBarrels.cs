﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UPGunBarrels : MonoBehaviour {
    [SerializeField]
    private int m_maxAmmo = 6;
    public int MaxAmmo { get { return m_maxAmmo; } }

    [SerializeField]
    private List<int> m_avaliableInLevels;
    public List<int> AvaliableInLevels { get { return m_avaliableInLevels; } }
}
