﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MovementBullet))]
public class Bullet : MonoBehaviour {
    [SerializeField]
    private float m_lifetime = 3f;

    [SerializeField, ReadOnly]
    private float m_lifetimeLeapse = 0f;

    public MovementBullet MovementBullet { get; private set; }

    private void Awake()
    {
        if (MovementBullet == null)
            MovementBullet = GetComponent<MovementBullet>();
    }

    private void Update()
    {
        if (m_lifetimeLeapse >= m_lifetime)
            Desativate();
        else
            m_lifetimeLeapse += Time.deltaTime;
    }
    
    public void Active(Vector3 position, Quaternion rotation, Movement parent = null, FX fx = FX.Shoot_Enemy)
    {
        MovementBullet.Rigidbody.velocity = Vector3.zero;
        MovementBullet.Rigidbody.angularVelocity = Vector3.zero;
        MovementBullet.Rigidbody.rotation = Quaternion.identity;

        transform.position = position;
        transform.rotation = rotation;

        gameObject.SetActive(true);

        SoundManager.Instance.AttachFX(gameObject, fx);

        if (parent == null)
            MovementBullet.ResetAcceleration();
        else
            MovementBullet.AdjustAcceleration(parent.Rigidbody.velocity.magnitude);
    }

    public void Desativate()
    {
        m_lifetimeLeapse = 0f;
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Desativate();
    }
}
