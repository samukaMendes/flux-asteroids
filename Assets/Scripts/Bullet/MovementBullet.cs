﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementBullet : Movement {

    private float m_originalAcceleration = 0f;
    private float m_originalVelocityLimit = 0f;

    protected override void Awake()
    {
        base.Awake();
        m_originalAcceleration = m_acceleration;
        m_originalVelocityLimit = m_velocityLimit;
    }

    private void OnEnable()
    {
        Move(transform.forward, ForceMode.Impulse);
    }

    private void OnDisable()
    {
        InstantBreakVelocity();
    }

    public void AdjustAcceleration(float parentCurrentSpeed)
    {
        if (m_velocityLimit < parentCurrentSpeed)
        {
            m_acceleration = m_originalAcceleration + parentCurrentSpeed;
            m_velocityLimit = m_originalVelocityLimit + parentCurrentSpeed;
        }
    }

    public void ResetAcceleration()
    {
        m_acceleration = m_originalAcceleration;
        m_velocityLimit = m_originalVelocityLimit;
    }
}
