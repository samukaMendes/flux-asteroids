﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Pooler))]
public class BulletSpawner : MonoBehaviour
{
    private int m_poolSize = 0;

    public Pooler Pooler { get; private set; }

    private void Awake()
    {
        if (Pooler == null)
            Pooler = GetComponent<Pooler>();

        m_poolSize = Pooler.ObjectPoolLimit;
    }

    private void OnEnable()
    {
        Pooler.AllDisabled += AllDisabled;
    }
    
    private void OnDisable()
    {
        Pooler.AllDisabled -= AllDisabled;
    }

    private void AllDisabled()
    {
        if(m_poolSize != Pooler.ObjectPoolLimit)
        {
            Pooler.UpdatePoolLimit(m_poolSize);
        }
    }

    public void Shoot(Transform spawnPoint, FX fx = FX.Shoot_Enemy)
    {
        GameObject bulletGameObject = Pooler.AvaliableObject;
        if (bulletGameObject != null)
        {
            Bullet bullet = bulletGameObject.GetComponent<Bullet>();
            bullet.Active(spawnPoint.position, spawnPoint.rotation, spawnPoint.GetComponentInParent<Movement>(), fx);
        }
    }

    public void QueuePoolUpdate(int newLimit)
    {
        m_poolSize = newLimit;
    }
}
