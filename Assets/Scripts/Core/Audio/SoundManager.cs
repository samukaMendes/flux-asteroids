﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance { get; private set; }

    [SerializeField]
    private SoundConfig m_soundConfig;
    public SoundConfig SoundConfig { get { return m_soundConfig; } }

    [SerializeField, ReadOnly]
    private List<AudioSource> m_fxAudioSource;

    private AudioSource AvaliableFXSource
    {
        get
        {
            for (int i = 0; i < m_fxAudioSource.Count; i++)
            {
                if (!m_fxAudioSource[i].isPlaying && m_fxAudioSource[i].clip == null)
                    return m_fxAudioSource[i];
            }

            return null;
        }
    }

    [SerializeField, ReadOnly]
    private List<AudioSource> m_bgmAudioSource;

    private AudioSource AvaliableBGMSource
    {
        get
        {
            for (int i = 0; i < m_bgmAudioSource.Count; i++)
            {
                if (!m_bgmAudioSource[i].isPlaying && m_bgmAudioSource[i].clip == null)
                    return m_bgmAudioSource[i];
            }

            return null;
        }
    }

    private AudioSource PlayingBGMSource
    {
        get
        {
            for (int i = 0; i < m_bgmAudioSource.Count; i++)
            {
                if (m_bgmAudioSource[i].isPlaying)
                    return m_bgmAudioSource[i];
            }

            return null;
        }
    }

    private void Awake()
    {
        if (Instance == null)
            InstanceInit();
    }

    private void InstanceInit()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);

        for (int i = 0; i < m_soundConfig.FXSourcePool; i++)
        {
            GameObject newAudio = new GameObject("FX Audio " + i);
            newAudio.transform.SetParent(transform, false);

            m_fxAudioSource.Add(newAudio.GetOrAddComponent<AudioSource>());
        }

        for (int i = 0; i < m_soundConfig.BGMSourcePool; i++)
        {
            GameObject newAudio = new GameObject("BGM Audio " + i);
            newAudio.transform.SetParent(transform, false);

            m_bgmAudioSource.Add(newAudio.GetOrAddComponent<AudioSource>());
        }
    }

    private void OnDestroy()
    {
        if (Instance == this)
            Instance = null;
    }

    #region FX
    public void PlayFX(FX fx, float delay = 0)
    {
        StartCoroutine(QueueFX(null, fx, null, delay));
    }

    public void AttachFX(GameObject gameObject, FX fx, float delay = 0)
    {
        //AudioSource source = gameObject.GetOrAddComponent<AudioSource>();
        //AudioSource source = AvaliableFXSource;
        StartCoroutine(QueueFX(gameObject.transform, fx, null, delay));
    }

    private IEnumerator QueueFX(Transform transform, FX fx, AudioSource source = null, float delay = 0)
    {
        while (source == null)
        {
            source = AvaliableFXSource;
            yield return null;
        }

        if (transform != null)
            source.transform.position = transform.position;

        source.clip = m_soundConfig.GetFXClip(fx);
        float length = source.clip.length;

        yield return new WaitForSeconds(delay);
        source.Play();
        yield return new WaitForSeconds(length);

        if (source != null)
            source.clip = null;
    }
    #endregion

    #region BGM
    public void PlayBGM(BGM bgm, float delay = 0)
    {
        StartCoroutine(QueueBGM(bgm, null, delay));
    }

    public void AttachBGM(GameObject gameObject, BGM bgm, float delay = 0)
    {
        AudioSource source = gameObject.GetOrAddComponent<AudioSource>();
        StartCoroutine(QueueBGM(bgm, source, delay));
    }

    public void SwitchBGM(BGM bgm, float fadeDuration, AudioSource to = null, AudioSource from = null, float delay = 0)
    {
        if (to == null)
            to = PlayingBGMSource;

        if (from == null)
            from = AvaliableBGMSource;

        if (to == null)
            StartCoroutine(QueueBGM(bgm, null, delay));
        else
            CrossFade(SoundConfig.GetBGMClip(bgm), to, from, fadeDuration);
    }

    private IEnumerator QueueBGM(BGM bgm, AudioSource source = null, float delay = 0)
    {
        while (source == null)
        {
            source = AvaliableBGMSource;
            yield return null;
        }

        source.clip = m_soundConfig.GetBGMClip(bgm);

        yield return new WaitForSeconds(delay);
        source.Play();
        yield return new WaitForSeconds(source.clip.length);
        source.clip = null;
    }
    #endregion

    #region Crossfade
    public void CrossFade(AudioClip clipToPlay, AudioSource to, AudioSource from, float fadingTime, float maxVolume = 1, float delay = 0)
    {
        StartCoroutine(Fade(clipToPlay, to, from, maxVolume, fadingTime, delay));
    }

    IEnumerator Fade(AudioClip playMe, AudioSource to, AudioSource from, float maxVolume, float fadingTime, float delay = 0)
    {
        yield return new WaitForSeconds(delay);

        from.clip = playMe;
        from.Play();
        from.volume = 0;

        StartCoroutine(FadeSource(to, to.volume, 0, fadingTime, true));
        StartCoroutine(FadeSource(from, from.volume, maxVolume, fadingTime));

        yield break;
    }

    IEnumerator FadeSource(AudioSource sourceToFade, float startVolume, float endVolume, float duration, bool stopAtEnd = false)
    {
        float elapsed = 0;

        while (elapsed < duration)
        {
            elapsed += Time.deltaTime;

            sourceToFade.volume = Mathf.Clamp01(Mathf.Lerp(startVolume, endVolume, elapsed / duration));

            yield return null;
        }

        if (stopAtEnd)
        {
            sourceToFade.clip = null;
            sourceToFade.Stop();
        }
    }
    #endregion
}
