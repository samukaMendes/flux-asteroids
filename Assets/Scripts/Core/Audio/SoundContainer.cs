﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Flags]
public enum FX
{
    Shoot_Player = 1 << 1,
    Shoot_Enemy = 1 << 2,
    Explosion_Player = 1 << 3,
    Explosion_Enemy = 1 << 4,
    Explosion_Asteroid = 1 << 5,
    GUI_Confirm = 1 << 6,
    GUI_Cancel = 1 << 7,
    GUI_Hover = 1 << 8,
}

[System.Flags]
public enum BGM
{
    Main_Menu = 1 << 1,
    Thriller = 1 << 2,
    WeAreNumberOne = 1 << 3,
    OceanMan = 1 << 4,
    TakeOnMe = 1 << 5,
}

[System.Serializable]
public class FXContainer
{
    [SerializeField]
    private FX m_fx;
    public FX FX { get { return m_fx; } }

    [SerializeField]
    private AudioClip m_clip;
    public AudioClip Clip { get { return m_clip; } }
}

[System.Serializable]
public class BGMContainer
{
    [SerializeField]
    private BGM m_BGM;
    public BGM BGM { get { return m_BGM; } }

    [SerializeField]
    private AudioClip m_clip;
    public AudioClip Clip { get { return m_clip; } }
}
