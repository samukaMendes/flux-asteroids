﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Sound Config", menuName = "Sound System/Sound Configuration", order = 20)]
public class SoundConfig : ScriptableObject {
    [Header("FX Configs")]
    [SerializeField]
    private int m_fxSourcePool = 4;
    public int FXSourcePool { get { return m_fxSourcePool; } }

    [SerializeField]
    private FXContainer[] m_fxTable;
    public FXContainer[] FXTable { get { return m_fxTable; } }

    [Header("BGM Configs")]
    [SerializeField]
    private int m_bgmSourcePool = 2;
    public int BGMSourcePool { get { return m_bgmSourcePool; } }

    [SerializeField]
    private BGMContainer[] m_bgmTable;
    public BGMContainer[] BGMTable { get { return m_bgmTable; } }

    public AudioClip GetFXClip(FX fx)
    {
        for (int i = 0; i < m_fxTable.Length; i++)
        {
            if (m_fxTable[i].FX.HasFlag(fx))
                return m_fxTable[i].Clip;
        }

        return null;
    }

    public AudioClip GetBGMClip(BGM bgm)
    {
        for (int i = 0; i < m_bgmTable.Length; i++)
        {
            if (m_bgmTable[i].BGM.HasFlag(bgm))
                return m_bgmTable[i].Clip;
        }

        return null;
    }
}
