﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warper : MonoBehaviour
{
    [Header("Horizontal Warp Offset")]
    [SerializeField, Range(0f, 1f)]
    private float m_horizontalOffset = 0.05f;

    [Header("Vertical Warp Offset")]
    [SerializeField, Range(0f, 1f)]
    private float m_verticalOffset = 0.05f;

    [SerializeField, ReadOnly]
    private bool m_isWarpingVertical = false;

    [SerializeField, ReadOnly]
    private bool m_isWarpingHorizontal = false;

    private void Update()
    {
        Vector3 newPosition = transform.position;
        m_isWarpingHorizontal = !transform.IsVisibleInHorizontal();
        m_isWarpingVertical = !transform.IsVisibleInVertical();

        if (m_isWarpingVertical)
        {
            newPosition.z = -newPosition.z;
        }

        if (m_isWarpingHorizontal)
        {
            newPosition.x = -newPosition.x;
        }

        if (transform.position != newPosition)
            Warp(newPosition);
    }

    private void Warp(Vector3 position)
    {
        Vector3 viewport = Camera.main.WorldToViewportPoint(position);
        if (viewport.x <= 0) viewport.x += m_horizontalOffset;
        if (viewport.x >= 1) viewport.x -= m_horizontalOffset;
        if (viewport.y <= 0) viewport.y += m_verticalOffset;
        if (viewport.y >= 0) viewport.y -= m_horizontalOffset;

        Vector3 landPosition = Camera.main.ViewportToWorldPoint(viewport);
        transform.position = landPosition;
    }
}
