﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Warper), typeof(Rigidbody))]
[System.Serializable]
public abstract class Movement : MonoBehaviour
{
    [SerializeField]
    protected float m_acceleration = 1f;

    [SerializeField]
    protected float m_torque = 3f;

    [SerializeField, ReadOnly]
    protected float m_velocity;

    [SerializeField]
    protected float m_velocityLimit = 10f;

    public Rigidbody Rigidbody { get; protected set; }

    protected virtual void Awake()
    {
        if (Rigidbody == null)
            Rigidbody = GetComponent<Rigidbody>();
    }

    protected virtual void Update()
    {
        m_velocity = Rigidbody.velocity.magnitude;
    }

    protected virtual void FixedUpdate()
    {
        if (Rigidbody.velocity.magnitude > m_velocityLimit)
            Rigidbody.velocity = Rigidbody.velocity.normalized * m_velocityLimit;
    }

    protected void Move(Vector3 direction, ForceMode forceMode = ForceMode.Acceleration)
    {
        Rigidbody.AddForce(direction * m_acceleration, forceMode);
    }

    protected void Rotate(Vector3 angles, ForceMode forceMode = ForceMode.Acceleration)
    {
        Rigidbody.AddTorque(angles * m_torque, forceMode);
    }

    protected void SlowDownVelocity(float time = 1f)
    {
        StartCoroutine(BreakVelocity(time));
    }

    private IEnumerator BreakVelocity(float time = 1f)
    {
        Vector3 originalVelocity = Rigidbody.velocity;
        float leapseTime = 0f;
        while (leapseTime < time)
        {
            Rigidbody.velocity = Vector3.Lerp(originalVelocity, Vector3.zero, leapseTime / time);
            leapseTime += Time.deltaTime;
            yield return null;
        }
    }

    protected void SlowDownTorque(float time = 1f)
    {
        StartCoroutine(BreakTorque(time));
    }

    private IEnumerator BreakTorque(float time = 1f)
    {
        Quaternion originalRotation = Rigidbody.rotation;
        float leapseTime = 0f;
        while (leapseTime < time)
        {
            Rigidbody.rotation = Quaternion.Slerp(originalRotation, Quaternion.identity, leapseTime / time);
            leapseTime += Time.deltaTime;
            yield return null;
        }
    }

    protected void InstantBreakVelocity()
    {
        Rigidbody.velocity = Vector3.zero;
    }

    protected void InstantBreakTorque()
    {
        Rigidbody.rotation = Quaternion.identity;
    }
}
