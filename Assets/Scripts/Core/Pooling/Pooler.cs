﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pooler : MonoBehaviour
{
    [SerializeField]
    private GameObject m_prefab;
    public GameObject Prefab { get { return m_prefab; } }

    [SerializeField]
    private Transform m_parent;
    public Transform Parent { get { return m_parent; } }

    [SerializeField]
    private bool m_autoGenerate = true;
    public bool AutoGenerate { get { return m_autoGenerate; } }

    [SerializeField]
    private int m_objectPoolLimit;
    public int ObjectPoolLimit { get { return m_objectPoolLimit; } }

    [SerializeField]
    private List<GameObject> m_objectPool;
    public List<GameObject> ObjectPool { get { return m_objectPool; } }

    public event System.Action PoolGenerated;
    public event System.Action AllEnabled;
    public event System.Action AllDisabled;

    private bool m_isCleaningPool = false;

    public bool IsAllDisabled
    {
        get
        {
            for (int i = 0; i < ObjectPool.Count; i++)
            {
                if (ObjectPool[i].activeInHierarchy)
                    return false;
            }

            return true;
        }
    }

    public bool IsAllEnabled
    {
        get
        {
            for (int i = 0; i < ObjectPool.Count; i++)
            {
                if (!ObjectPool[i].activeInHierarchy)
                    return false;
            }

            return true;
        }
    }

    public GameObject AvaliableObject
    {
        get
        {
            if (m_isCleaningPool)
                return null;

            for (int i = 0; i < m_objectPool.Count; i++)
            {
                if (!m_objectPool[i].activeInHierarchy)
                {
                    return m_objectPool[i];
                }
            }

            return null;
        }
    }

    public GameObject[] AvaliableObjects(int maxObjects)
    {
        if (m_isCleaningPool)
            return null;

        List<GameObject> objectsList = new List<GameObject>();

        for (int i = 0; i < m_objectPool.Count; i++)
        {
            if (!m_objectPool[i].activeInHierarchy && objectsList.Count < maxObjects)
            {
                objectsList.Add(m_objectPool[i]);
            }
        }

        return objectsList.ToArray();
    }

    public void ActivateAll(bool active = true)
    {
        for (int i = 0; i < m_objectPool.Count; i++)
        {
            m_objectPool[i].SetActive(active);
        }
    }

    private void Awake()
    {
        if (m_parent == null)
            m_parent = transform;
    }

    private void Start()
    {
        if (m_autoGenerate)
            GeneratePool();
    }

    private void Update()
    {
        if (m_objectPool.Count != m_objectPoolLimit && !m_isCleaningPool)
            UpdatePoolLimit(m_objectPoolLimit);

        if (AllEnabled != null && IsAllEnabled)
            AllEnabled.Invoke();

        if (AllDisabled != null && IsAllDisabled)
            AllDisabled.Invoke();
    }

    public void UpdatePoolLimit(int newLimit, bool regerate = true)
    {
        m_isCleaningPool = true;

        if (ObjectPool.Count > 0)
            CleanPool();

        m_objectPoolLimit = newLimit;

        if (regerate)
            GeneratePool();

        m_isCleaningPool = false;
    }

    private void CleanPool()
    {
        for (int i = 0; i < m_objectPool.Count; i++)
        {
            Destroy(m_objectPool[i].gameObject);
        }
    }

    public GameObject[] GeneratePool()
    {
        GameObject[] gameObjectsPool = new GameObject[m_objectPoolLimit];
        for (int i = 0; i < m_objectPoolLimit; i++)
        {
            gameObjectsPool[i] = Instantiate(m_prefab, m_parent);
            gameObjectsPool[i].SetActive(false);
        }

        m_objectPool = new List<GameObject>(gameObjectsPool);

        if (PoolGenerated != null)
            PoolGenerated.Invoke();

        return gameObjectsPool;
    }
}
