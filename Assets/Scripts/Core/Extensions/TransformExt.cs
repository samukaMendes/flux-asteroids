﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExt {
    public static bool IsVisibleToCamera(this Transform transform, Camera camera = null)
    {
        if (camera == null)
            camera = Camera.main;

        Vector3 viewport = camera.WorldToViewportPoint(transform.position);
        return (viewport.x >= 0 && viewport.y >= 0) && (viewport.x <= 1 && viewport.y <= 1) && viewport.z >= 0;
    }

    public static bool IsVisibleInHorizontal(this Transform transform, Camera camera = null)
    {
        if (camera == null)
            camera = Camera.main;

        Vector3 viewport = camera.WorldToViewportPoint(transform.position);
        return viewport.x >= 0 && viewport.x <= 1;
    }

    public static bool IsVisibleInVertical(this Transform transform, Camera camera = null)
    {
        if (camera == null)
            camera = Camera.main;

        Vector3 viewport = camera.WorldToViewportPoint(transform.position);
        return viewport.y >= 0 && viewport.y <= 1;
    }
}
