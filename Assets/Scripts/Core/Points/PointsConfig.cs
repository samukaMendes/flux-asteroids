﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Point Config", menuName = "Point System/Point Config", order = 20)]
public class PointsConfig : ScriptableObject
{
    [SerializeField]
    private int m_maxRegisters = 10;
    public int MaxRegisters { get { return m_maxRegisters; } }

    [SerializeField]
    private PointsContainer[] m_defaultScore;
    public PointsContainer[] DefaultScore { get { return m_defaultScore; } }

    [SerializeField]
    private PointsContainer[] m_savedScore;
    public PointsContainer[] SavedScore { get { return m_savedScore; } }

    private void Awake()
    {
        if (m_savedScore == null || m_savedScore.Length == 0)
            RestoreDefault();
    }

    public bool IsOnHighScore(int score)
    {
        int position = -1;
        for (int i = m_savedScore.Length - 1; i >= 0; i--)
        {
            if (score > m_savedScore[i].Score)
                position = i;
        }
        return position >= 0 ? true : false;
    }

    public int HighScorePosition(int score)
    {
        int position = -1;
        for (int i = m_savedScore.Length - 1; i >= 0; i--)
        {
            if (score > m_savedScore[i].Score)
                position = i;
        }
        
        return position;
    }

    public void RegisterHighScore(string name, int score)
    {
        if (!IsOnHighScore(score))
            return;

        int position = HighScorePosition(score);

        PointsContainer[] points = new PointsContainer[m_maxRegisters];
        PointsContainer newRegister = new PointsContainer(name, score);
        int savedPosition = 0;

        for (int i = 0; i < m_maxRegisters; i++)
        {
            if (newRegister != null)
            {
                if (i == position)
                {
                    points[i] = newRegister;
                    savedPosition--;
                }
                else
                    points[i] = m_savedScore[savedPosition];
            }

            savedPosition++;
        }

        m_savedScore = points;
    }

    [ContextMenu("Restore default")]
    private void RestoreDefault()
    {
        if (m_defaultScore == null || m_defaultScore.Length == 0)
            GenerateDefault();

        m_savedScore = m_defaultScore;
    }

    [ContextMenu("Generate default")]
    private void GenerateDefault()
    {
        PointsContainer[] pointsContainers = new PointsContainer[m_maxRegisters];
        int inverseControll = 1;
        for (int i = m_maxRegisters - 1; i >= 0; i--)
        {
            pointsContainers[i] = new PointsContainer("Player " + (i + 1), 500 * inverseControll);
            inverseControll++;
        }

        m_defaultScore = pointsContainers;
    }
}
