﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PointsContainer
{
    [SerializeField]
    private string m_name;
    public string Name { get { return m_name; } }

    [SerializeField]
    private int m_score;
    public int Score { get { return m_score; } }

    public PointsContainer(string name, int score)
    {
        m_name = name;
        m_score = score;
    }
}
